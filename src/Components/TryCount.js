import React from 'react';

const TryCount = props => {

    return (
        <div>
            <p>Tries: {props.count}</p>
        </div>
    );
};

export default TryCount;