import React, { Component } from 'react';
import './App.css';
import Button from "./Components/Button";
import TryCount from "./Components/TryCount";

class App extends Component {

    Board = () => {
        let btns = [];
        for (let i = 0; i < 36; i++) {
            btns.push({isCliked: false, ring:false, showRing: false})
        }
        let ring = Math.floor(Math.random() * btns.length);
        btns[ring].ring = true;

        return btns;
    };

    state = {
        buttons: this.Board(),
        count: 0,
        finishedGame: false

    };


    boardClicked = id => {
        console.log(id);
        let buttons = this.state.buttons;
        buttons[id].isCliked = true;
        if (buttons[id].ring) {
            buttons[id].showRing = true;
            buttons = buttons.map(item => ({...item, isCliked: true}) );
            this.setState({buttons, finishedGame: true});
        }
        this.setState({buttons, count: this.state.count + 1});
    };

    resetGame = () =>{
        this.setState({buttons : this.Board(), count: 0, finishedGame: false})
    };



    render() {

        return (
            <div className="App">
                {this.state.buttons.map((btn, key) => <Button
                    showRing={btn.showRing? "showRing" : null}
                    class={btn.isCliked? "disabled" : null}
                    click={ () => this.boardClicked(key)}
                    disabled={btn.isCliked}
                    key={key}
                />)}
                <TryCount count={this.state.count}/>
                {/*<Button reset={this.resetGame} className="btn-reset">Reset</Button>*/}
                <button onClick={this.resetGame} className="btn-reset">Reset</button>
                {/*{this.state.finishedGame && alert("игра окончена")}*/}
            </div>
        );
    }
}

export default App;

